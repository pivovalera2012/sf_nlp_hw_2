# Домашнее задание №2. Разработка генеративного чат-бота

## Цель работы
Цель работы - это разработать чат-бот, используя генеративный подход. Бот должен вести диалог как определенный персонаж сериала, имитируя стиль и манеру конкретного персонажа сериала. Важно учесть особенности речи и темы, которые поднимает персонаж, его типичные реакции.

## Данные для обучения
Целевым персонажем на которого должен быть похож чат бот был выбран Доктор Грегори Хаус - главный герой американского телесериала «Доктор Хаус»

![1](https://gitlab.com/pivovalera2012/sf_nlp_hw_2/-/raw/main/data/dr_House.jpeg)

Доктор Хаус — блестящий диагност со специализацией в двух основных областях: заболевания почек и инфекционные болезни. Однако из-за некоторых особенностей характера он не является любимцем пациентов и коллег: он замкнут, резок и циничен, склонен к бунтарству. Он не обременяет себя соблюдением правил хорошего тона, и временами кажется, что он полностью лишён чувства сострадания (что, однако, неверно). Лучше всего говорят о жизненной позиции Хауса слова доктора Формана: «Он не нарушает правила, он их игнорирует», его любимое утверждение — «Все лгут». Именно этот подход нередко помогает ему разобраться в самых сложных и невероятных случаях, а значит, спасти жизнь ещё одному пациенту.

Когда-то Хаус перенёс инфаркт четырёхглавой мышцы правого бедра, был прооперирован, и теперь испытывает постоянные сильные боли в ноге, от которых спасается, принимая викодин. Ходит тяжело, опираясь на трость, принимает викодин постоянно в огромных дозах, зачастую — прямо в присутствии пациентов, без викодина он не в состоянии ни работать, ни просто жить; фактически он наркоман, знающий о своей зависимости, но не желающий от неё избавляться. 

Идеальный кандидат! Ведь, что может быть лучше грубящего и хамящего чат-бота?

В качестве данных были взяты транскрипции сериала с [Kaggle](https://www.kaggle.com/datasets/kunalbhar/house-md-transcripts?resource=download&select=season2.csv).\
Пример данных:
- Wilson, Is there any other kind?
- House, What're you doing?
- Wilson, There's a sale on Liquid Tide.
- House," If you're broke, I can lend you a tiny bit of the money I owe you."
- Wilson," No, no, I wouldn't put you in that position. What does the diary say?"
- House," It's basically a list of her sexual encounters. Boys, girls, vibrating appliances."


В ходе внимателньеого рассмотрения данных было обнаружено, что данные требуют небольшой очистки, поскольку в них присутсвуют неизвестные символы, а так же описание почихдящего в квадратных скобках.

- Пример до обработки: '"D"\x9d, see ya. [Crosses out the "D"\x9d] "N"\x9d for neoplastic?'
- Пример после обработки: '"D", see ya.  "N" for neoplastic?'



## Базовые модели

[DialoGPT](https://huggingface.co/microsoft/DialoGPT-medium) - чат-бот с искусственным интеллектом от Microsoft, обученный на 147 млн. многооборотных диалогов с Reddit. Это крупномасштабная предварительно обученная SOTA модель генерации диалоговых ответов для многооборотных бесед. Результаты оценки человека показывают, что ответ, генерируемый с помощью DialoGPT, сопоставим с качеством ответа человека в тесте Тьюринга для однооборотной беседы.

Это первая модель с помощько которой я хотел решить поставленную задачу. Однако после неудовлетоврительных результатов было принято решение взять другую модель.

[LLaMA-2](https://huggingface.co/meta-llama/Llama-2-7b-chat-hf) - это большая языковая модель (LLM), от Meta AI, которая помогает понимать вводимые человеком данные и реагировать на них, а также разрабатывать текст, похожий на человеческий. Llama 2 использует модель transformer для обучения. Llama обучается на больших наборах данных, которые представлены в текстовых форматах. Llama 2 может похвастаться расширенными возможностями с точки зрения понимания языка, генерации и точной настройки. Модель использует более крупный и разнообразный набор данных, что позволяет ему эффективно выполнять широкий спектр задач.

## Дообучение моделей

Код обработки данных и дообчения предствален в файле [model_train](https://gitlab.com/pivovalera2012/sf_nlp_hw_2/-/blob/main/model_train.ipynb?ref_type=heads)

### Первая версия дообучения LLaMa-2

Создание первой версии проходило при помощи данных взятых только из первого сезона сериала.\
Отличие от последующих версий - это вид промпта, который был составлен следующим образом: 'Контекст0' + 'Контекст1' + 'Answer like Dr. House:' + 'Ответ'

Параметры в процессе обчения предствалены на графике
![График](https://gitlab.com/pivovalera2012/sf_nlp_hw_2/-/raw/main/data/fig1.png?ref_type=heads)

### Результат

В результате получился бот который сумел уловить особенности Доктора Хауса. Пример:
   - Вопрос: What is the universe?
   - Ответ: It's a big, cold, empty place. It's not a question of what it is, it's a question of what we want it to be.  And right now, I want it to be a place where I can find the next Vicodin.  You know, for my leg.

или такой
   - We need to do an MRI scan on him otherwise he will die.
   - "MRI? Oh, please. That's like the first thing that comes to mind when you hear the word 'death'. Like, 'Oh no, we need to do an MRI scan or he'll die!' No, no, no. I've seen plenty of patients who've died without ever getting an MRI scan. It's not like that's the magic bullet. Although, I suppose it could be useful in determining the cause

Ответ однозначно подстать нашему персонажу!

Однако по мере увеличения контекста или при попытке задать очень которкий вопрос наблюдается многократное повторение фразы "Answer like Dr. House" что не очень хорошо.
После изучения данного впопроса, было обнарущено, что у LLaMa-2 испольуется специалный вид промпта, что получило отражение в следующей версии.\
[Ссылка](https://huggingface.co/pivovalera2012/Llama-2-7b-Dr-House) на дообученную HuggingFace модель

### Вторая версия дообучения LLaMa-2

Создание второй версии проходило при помощи данных взятых только из первого и второго сезона сериала.\

В этой версии промт выглядел следующим образом: [INST]  Запрос [/INST]  Ответ.

Примеры:
"[INST]  What's that? What are you doing? [/INST]  Painkillers. "
"[INST]  Yes, yes, her name is Rebecca. I call her Rachel. [/INST]  You idiot! [INST]  Hey!listen! [/INST]  Not you, him! You said you didn't find anything."

Параметры в процессе обчения предствалены на графике
![График](https://gitlab.com/pivovalera2012/sf_nlp_hw_2/-/raw/main/data/fig2.png?ref_type=heads)

[Ссылка](https://huggingface.co/pivovalera2012/Llama-2-7b-Dr-House_v2) на дообученную HuggingFace модель

### Результат

Результаты оказались не совсем однозначными. Модель вела себя как-будто никакого дообучения и не происходило вовсе. Я не смог выявить никаких особенных оборотов речи присущих доктору Хаусу. По этой причине быле решено увелисить котичество данных и изменить немного параметры LoRa

### Третья версия дообучения LLaMa-2

Создание третьей версии проходило при помощи данных взятых из первых четырех сезона сериала.\
Промпт остался как и во второй версии.

Так же помимо ихменения количества оданных для обучения был повышена размерность внимания LoRa (r - int) в peft.LoraConfig:
 - В первых двух версиях он равен 2
 - В третей весии он равен 32

Обчуение заняло около 8 часов. Параметры в процессе обчения предствалены на графике
![График](https://gitlab.com/pivovalera2012/sf_nlp_hw_2/-/raw/main/data/fig3.png?ref_type=heads)

[Ссылка](https://huggingface.co/pivovalera2012/Llama-2-7b-Dr-House_v3) на дообученную HuggingFace модель

### Результат

Примеры ответов бота:
 - Why aren't you working?
 - Oh, great. Another whiny little brat who wants my time for free. Look, kid, I didn't become one of the smartest guys in the world just to sit around all day doing nothing. I have better things to do than waste my time talking to idiots like you. And yes, I mean worse things. Now either bug off, or be prepared to get screwed with some epic tests. Your choice. Are you going to go away now, or do you need more reasons why you should leave me alone? 

 - We need to do an MRI scan on him otherwise he will die
 - Hmmm. An MRI scan. Interesting. My first question is, why do you think you need to do an MRI scan on this guy? And don't say "Because his leg hurts!" unless you want to risk looking like a complete moron. You gotta be kiddin', right? I mean, come on, I've seen more brains in these three minutes than most of these med students have seen their whole lives. The only reason they let you into this hospital was so I could fix your pathetic excuse for a brain. Not to play God with a poor bastard who needs a real diagnostic test. So, spill the beans. Why the hell do you need an MRI?

Чтож очень похоже на нешего героя. однако все равно в процессе работы переодически наблюдаются ответы более присущие базовой модели: Например при попытке попросить поставить диагноз, чат сразу говорит что он не доктор.

## Веб-серсис

Для того чтобы опробовать чат-бот необходимо просто запутсить файл [app.py](https://gitlab.com/pivovalera2012/sf_nlp_hw_2/-/blob/main/app.py?ref_type=heads)

Интерфейс веб-сервиса представлен на картинке

![app](https://gitlab.com/pivovalera2012/sf_nlp_hw_2/-/raw/main/data/app.png?ref_type=heads)

## Выводы

В результате обучения всех версий (см. графики) наблюдаяется типичная картина уменшения значения loss

В ответах модели присутсвуют типичные реакции Хауса: назвать всех идиотами или ответиь что ей плевать на что-то. Есть так же и сарказм с издевательскими нотками.
По итогу нельзя сказать, что вышел полноценный персональный доктор Хаус, но определенно часть его запечатлилась в процессе обучения:

 - В первой версии получилист очень похожие ответы, в стиле доктора Хауса, но при этом плохо улавливается контекс и через некоторое время начинает много раз пофторять какую-нибудь чать ответа.
 - Вторая версия стала переходной
 - А третья смогла полноценно реализоваться как чат-бот, но при этом иногда ей присущи ответы не в стиле моего персонажа.




